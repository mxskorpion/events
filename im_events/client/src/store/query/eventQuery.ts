import eventStore from "../event.store";


export default class EventListQuery {
  static getEventList() {
    fetch('http://localhost:8180/im_events/getAllEvents', {method: "GET"})
      .then(response => response.json()).then(data => { 
        eventStore.eventList = data});
  }
}