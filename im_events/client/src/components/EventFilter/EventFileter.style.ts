export const filterButtonStyle = {
  borderRadius: "15px",
  border: "2px solid #adb5bd",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  fontFamily: "Benzin-Regular",
  fontSize: "18px",
  fontWeight: 600,
  color: "#8b8c89",
};

export const eventFilterWrapperStyle = {
  display: "flex",
  justifyContent: "center",
  flexWrap: "wrap" as const,
  gap: "30px",
  paddingTop: "30px",
};
