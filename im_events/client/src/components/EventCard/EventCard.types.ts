export interface IEventCard {
  eventDate: string;
  eventName: string;
  eventCoins: number;
  category: string;
}
