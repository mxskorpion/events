# Установка зависимостей

```bash
yarn
```

или

```bash
npm install
```

# Запуск в режиме разработки

```bash
yarn start
```

или

```bash
npm run start
```

---

# Сборка проекта

```bash
yarn build
```

или

```bash
npm run build
```
