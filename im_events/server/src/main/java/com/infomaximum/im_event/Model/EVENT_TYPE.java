package com.infomaximum.im_event.Model;

/**
 * Created by a.zevaykin
 * Date: 21.07.2023
 */
public enum EVENT_TYPE {
    SPORT,
    EDUCATION,
    CULTURE,
    GAME,
    CINEMA,
    CHILL

}
